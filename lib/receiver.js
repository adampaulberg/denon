const Denon = require('denon-client');
const config = require('../config');
const particle = require('./particle');

const client = new Denon.DenonClient(config.host);

const inputMap = {
    'playstation': Denon.Options.InputOptions.Cable,
    'chromecast': Denon.Options.InputOptions.MPlay,
    'xbox': Denon.Options.InputOptions.DVD
}

async function getConnection(){
    try{
        await client.connect();
        return client;
    } catch(e) {
        console.log(e);
    }
}

async function getPowerState() {
    try{
        let connection = await getConnection();
        let status = await connection.getPower();

        return status;
    } catch (e) {
        console.log(e);
    }
}

async function getVolume() {
    try {
        let connection = await getConnection();
        let volume = await connection.getVolume();

        return volume;
    } catch(e) {
        console.log(e);
    }
}

async function power(state) {
    try{
        let connection = await getConnection();
        
        if(state) {
            await particle.callParticleFunction('turnProjectorOn');
            return await connection.setPower(Denon.Options.PowerOptions.On);
        } else {
            await particle.callParticleFunction('turnProjectorOff');            
            return await connection.setPower(Denon.Options.PowerOptions.Standby);
        }
    } catch(e) {
        console.log(e);
    }
}

async function input(input) {
    input = input || '';
    input = input.toLowerCase();
    
    let connection = await getConnection();
    
    let receiverInput = inputMap[input];

    connection.setInput(receiverInput);
}

async function volume(level) {
    let connection = await getConnection();
    await connection.setVolume(level);

}

module.exports = {
    getConnection: getConnection,
    getPowerState: getPowerState,
    getVolume: getVolume,
    power: power,
    input: input,
    volume: volume,
    options: Denon.Options
};