let Particle = require('particle-api-js');
let particle = new Particle();

let config = require('../config');

async function callParticleFunction(name) {
    let token = await particle.login({
        username: config.particle.user, 
        password: config.particle.password
    });

    let result = await particle.callFunction({ 
        deviceId: config.particle.deviceId, 
        name: name, 
        argument: '', 
        auth: token.body.access_token
    });
}

module.exports = {
    callParticleFunction: callParticleFunction
};