const receiver = require('./receiver');

async function input(name) {
    try{
        await power('on');
        console.log('input', name)
        await receiver.input(name);
    } catch(e) {
        console.log(e);
    }
}

async function power(status) {
    try{
        status = status || '';
        let power_status = await receiver.getPowerState();

        console.log('current status', power_status)

        if(status == 'on') {
            console.log('turning on receiver');
            if(power_status != 'ON') {
                await receiver.power(true);
            }
        }
        else if(status == 'off') {
            console.log('turning off receiver');            
            await receiver.power(false);
        } else {
            console.log('power option unknown', status);
        }
    } catch (e) {
        console.log(e);
    }
}

async function volume(level) {
    try {
        await receiver.volume(level);
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    input: input,
    power: power,
    volume: volume
};