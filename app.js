const express = require('express');
const morgan = require('morgan');

const config = require('./config');
const commands = require('./lib/commands');

const app = express();

app.use(morgan('dev'));

app.get('/', (req, res, next) => {
    res.send('ok');
});

app.get('/input/:input', async (req, res, next) => {
    await commands.input(req.params.input);

    return res.send('ok');
});

app.get('/power/:status', async (req, res, next) => {
    await commands.power(req.params.status);

    return res.send('ok');
});

app.get('/volume/:volume', async(req, res, next) => {
    await commands.volume(req.params.volume);

    return res.send('ok');
});

app.listen(config.port, () => console.log('Denon App Online'))